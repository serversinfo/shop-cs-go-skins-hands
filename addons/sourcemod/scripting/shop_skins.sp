#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <shop>
#include <tp_preview>
#pragma newdecls required

#define VERSION "2.2.135"

#define CATEGORY	"skins"

KeyValues kv;
//ItemId g_selected_id[MAXPLAYERS+1] = {INVALID_ITEM, ...};
int g_iIDsTeam[2048];			// к какой команде принадлежит скин, 1-обе, 2-т, 3-кт
ItemId g_selected_id_t[MAXPLAYERS+1] = {INVALID_ITEM, ...};
ItemId g_selected_id_ct[MAXPLAYERS+1] = {INVALID_ITEM, ...};

Handle hArrayModels;

public Plugin myinfo =
{
	name		= "[Shop] Skins",
	author		= "FrozDark",
	description = "Adds ability to buy skins",
	version		= VERSION,
	url			= "www.hlmod.ru"
}

public void OnPluginStart()
{
	HookEvent("player_spawn", Event_PlayerSpawn);
	HookEvent("player_team", Event_PlayerSpawn);
	
	hArrayModels = CreateArray(ByteCountToCells(PLATFORM_MAX_PATH));
	
	if (Shop_IsStarted()) Shop_Started();
}

public void OnPluginEnd()
{
	Shop_UnregisterMe();
}

public void OnMapStart()
{
	char buffer[PLATFORM_MAX_PATH];
	for (int i=0; i<GetArraySize(hArrayModels); i++) {
		GetArrayString(hArrayModels, i, buffer, sizeof(buffer));
		PrecacheModel(buffer, true);
	}
	
	Shop_GetCfgFile(buffer, sizeof(buffer), "skins_downloads.txt");
	
	if (!File_ReadDownloadList(buffer))
		PrintToServer("File not exists %s", buffer);
}

public void OnClientDisconnect_Post(int client)
{
	g_selected_id_t[client] = INVALID_ITEM;
	g_selected_id_ct[client] = INVALID_ITEM;
}

public void Shop_Started()
{
	CategoryId category_id = Shop_RegisterCategory(CATEGORY, "Скины", "");
	
	char _buffer[PLATFORM_MAX_PATH];
	Shop_GetCfgFile(_buffer, sizeof(_buffer), "skins.txt");
	
	if (kv != INVALID_HANDLE) CloseHandle(kv);
	
	kv = CreateKeyValues("Skins");
	
	if (!FileToKeyValues(kv, _buffer))
		ThrowError("\"%s\" not parsed", _buffer);
	
	ClearArray(hArrayModels);
	
	KvRewind(kv);
	char sItem[64], item_name[64], desc[64];
	int iTeam;
	if (KvGotoFirstSubKey(kv)) {
		do {
			if (!KvGetSectionName(kv, sItem, sizeof(sItem))) continue;
			
			KvGetString(kv, "ModelT", _buffer, sizeof(_buffer));
			bool result = false;
			if (_buffer[0]) {
				iTeam = 2;
				PrecacheModel(_buffer);
				if (FindStringInArray(hArrayModels, _buffer) == -1)
					PushArrayString(hArrayModels, _buffer);
				//KvGetString(kv, "ModelT_Arms", _buffer, sizeof(_buffer));
				//if (_buffer[0]) {
				//	PrecacheModel(_buffer);
				//	//PrintToChatAll("%s precahsed", _buffer);
				//}
				result = true;
			}
			KvGetString(kv, "ModelCT", _buffer, sizeof(_buffer));
			if (_buffer[0]) {
				if (iTeam == 2) iTeam =1;
				else iTeam = 3;
				PrecacheModel(_buffer, true);
				if (FindStringInArray(hArrayModels, _buffer) == -1)
					PushArrayString(hArrayModels, _buffer);
				//KvGetString(kv, "ModelCT_Arms", _buffer, sizeof(_buffer));
				//if (_buffer[0]) {
				//	PrecacheModel(_buffer);
				//}
			} else if (!result) continue;
			g_iIDsTeam[category_id] = iTeam;
			if (Shop_StartItem(category_id, sItem)) {
				Shop_GetItemId(category_id, sItem);
				KvGetString(kv, "name", item_name, sizeof(item_name), sItem);
				KvGetString(kv, "description", desc, sizeof(desc));
				int iPrice = KvGetNum(kv, "price", 5000);
				Shop_SetInfo(item_name, desc, iPrice, KvGetNum(kv, "sell_price", 2500), Item_Togglable, KvGetNum(kv, "duration", 86400));
				if (0<iPrice<100000000)
					Shop_SetCallbacks(_, OnEquipItem, _, _, _, OnPreviewItem);
				else Shop_SetCallbacks(_, OnEquipItem);
				
				if (KvJumpToKey(kv, "Attributes", false)) {
					Shop_KvCopySubKeysCustomInfo(kv);
					KvGoBack(kv);
				}
				Shop_EndItem();
			}
		} while (KvGotoNextKey(kv));
	}
	KvRewind(kv);
}

public void OnPreviewItem(int client, CategoryId category_id, const char[] category, ItemId item_id, const char[] item)
{
	char buffer[PLATFORM_MAX_PATH];
	
	KvRewind(kv);
	if (!KvJumpToKey(kv, item, false)) {
		LogError("It seems that registered item \"%s\" not exists in the settings", buffer);
		return;
	}

	switch (GetClientTeam(client)) {
		case 2 : KvGetString(kv, "ModelT", buffer, sizeof(buffer));
		case 3 : KvGetString(kv, "ModelCT", buffer, sizeof(buffer));
		default : buffer[0] = 0;
	}

	KvRewind(kv);

	if (buffer[0] && IsModelFile(buffer)) {
		Mirror(client, buffer);
		CreateTimer(5.0, SetBackMode, GetClientUserId(client), TIMER_FLAG_NO_MAPCHANGE);
		SetTP(client, true, true, 5.0);
	}
	return;
}

public Action SetBackMode(Handle timer, any userid)
{
	int client = GetClientOfUserId(userid);
	if(client)
		Mirror(client);
}

void Mirror(int client, const char[] sModel="")
{
	if (sModel[0]) {
		SetTP(client, true, true);
		SetEntityModel(client, sModel);
	} else ProcessPlayer(client);
}

public ShopAction OnEquipItem(int client, CategoryId category_id, const char[] category, ItemId item_id, const char[] item, bool isOn, bool elapsed)
{
	if (isOn || elapsed) {
		CS_UpdateClientModel(client);
		
		if (g_iIDsTeam[category_id] == 2)
			g_selected_id_t[client] = INVALID_ITEM;
		else if (g_iIDsTeam[category_id] == 3)
			g_selected_id_ct[client] = INVALID_ITEM;
		else {
			g_selected_id_t[client] = INVALID_ITEM;
			g_selected_id_ct[client] = INVALID_ITEM;
		}
		return Shop_UseOff;
	}
	// g_iIDsTeam[category_id] == GetClientTeam(client)
	Shop_ToggleClientCategoryOff(client, category_id);			// Нужно сделать чтобы он не отключал все предметы, т.к. может быть у игрока до этого был скин за другую команду
	switch(g_iIDsTeam[category_id]) {
		case 1: {
			g_selected_id_t[client] = item_id;
			g_selected_id_ct[client] = item_id;
		}
		case 2: g_selected_id_t[client] = item_id;
		case 3: g_selected_id_ct[client] = item_id;
	}
	ProcessPlayer(client, true);
	return Shop_UseOn;
}

public void Event_PlayerSpawn(Event e, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(e.GetInt("userid"));
	CS_UpdateClientModel(client);
	CreateTimer(0.3, ProcessPlayer_tmr, client, TIMER_FLAG_NO_MAPCHANGE);
}

public Action ProcessPlayer_tmr(Handle timer, any client)
{
	ProcessPlayer(client);
}

void ProcessPlayer(int client, bool bPrew=false)
{
	if (client && !IsFakeClient(client) && IsPlayerAlive(client)) 
		if ((GetClientTeam(client) == 2)? (g_selected_id_t[client] != INVALID_ITEM):(g_selected_id_ct[client] != INVALID_ITEM)) {
			
			if (IsClientInGame(client)) {
				int iTeam = GetClientTeam(client);
				if((iTeam == 2)? (g_selected_id_t[client] != INVALID_ITEM):(g_selected_id_ct[client] != INVALID_ITEM)) {
					char buffer[PLATFORM_MAX_PATH];
					Shop_GetItemById((iTeam == 2)? g_selected_id_t[client]:g_selected_id_ct[client], buffer, sizeof(buffer));
					KvRewind(kv);
					if (!KvJumpToKey(kv, buffer, false)) {
						LogError("It seems that registered item \"%s\" not exists in the settings", buffer);
						return;
					}
					
					switch (iTeam) {
						case 2 :  KvGetString(kv, "ModelT", buffer, sizeof(buffer));
						case 3 :  KvGetString(kv, "ModelCT", buffer, sizeof(buffer));
						default : buffer[0] = '\0';
					}
					if (buffer[0] && IsModelFile(buffer))
						SetEntityModel(client, buffer);
					KvRewind(kv);
				}
				if (bPrew)
					SetTP(client, true, true, 5.0);
			}
		} else CS_UpdateClientModel(client);
}

//public Action Timer_SetClientModel(Handle timer, any client)
//{
//	if (IsClientInGame(client)) {
//		int iTeam = GetClientTeam(client);
//		if((iTeam == 2)? (g_selected_id_t[client] != INVALID_ITEM):(g_selected_id_ct[client] != INVALID_ITEM)) {
//			char buffer[PLATFORM_MAX_PATH];
//			Shop_GetItemById((iTeam == 2)? g_selected_id_t[client]:g_selected_id_ct[client], buffer, sizeof(buffer));
//			KvRewind(kv);
//			if (!KvJumpToKey(kv, buffer, false)) {
//				LogError("It seems that registered item \"%s\" not exists in the settings", buffer);
//				return;
//			}
//			
//			switch (iTeam) {
//				case 2 :  KvGetString(kv, "ModelT", buffer, sizeof(buffer));
//				case 3 :  KvGetString(kv, "ModelCT", buffer, sizeof(buffer));
//				default : buffer[0] = '\0';
//			}
//			if (buffer[0] && IsModelFile(buffer))
//				SetEntityModel(client, buffer);
//			KvRewind(kv);
//		}
//		SetTP(client, true, true, 5.0);
//	}
//}

bool IsModelFile(const char[] model)
{
	char buf[4];
	File_GetExtension(model, buf, sizeof(buf));
	
	return !strcmp(buf, "mdl", false);
}

char _smlib_empty_twodimstring_array[][] = { { '\0' } };
stock void File_AddToDownloadsTable(char[] path, bool recursive=true, const char[][] ignoreExts=_smlib_empty_twodimstring_array, int size=0)
{
	if (path[0] == '\0')
		return;
	
	int len = strlen(path)-1;
	
	if (path[len] == '\\' || path[len] == '/') 
		path[len] = '\0'; 

	if (FileExists(path)) {
		
		char fileExtension[4];
		File_GetExtension(path, fileExtension, sizeof(fileExtension));
		
		if (StrEqual(fileExtension, "bz2", false) || StrEqual(fileExtension, "ztmp", false))
			return;
		
		if (Array_FindString(ignoreExts, size, fileExtension) != -1)
			return;

		AddFileToDownloadsTable(path);
		
		if (StrEqual(fileExtension, "mdl", false)) 
			PrecacheModel(path, true); 
	} else if (recursive && DirExists(path)) {
		char dirEntry[PLATFORM_MAX_PATH];
		Handle __dir = OpenDirectory(path);

		while (ReadDirEntry(__dir, dirEntry, sizeof(dirEntry))) {

			if (StrEqual(dirEntry, ".") || StrEqual(dirEntry, ".."))
				continue;
			Format(dirEntry, sizeof(dirEntry), "%s/%s", path, dirEntry);
			File_AddToDownloadsTable(dirEntry, recursive, ignoreExts, size);
		}
		CloseHandle(__dir);
	} else if (FindCharInString(path, '*', true)) {
		char fileExtension[4];
		File_GetExtension(path, fileExtension, sizeof(fileExtension));

		if (StrEqual(fileExtension, "*")) {
			char dirName[PLATFORM_MAX_PATH],
				fileName[PLATFORM_MAX_PATH],
				dirEntry[PLATFORM_MAX_PATH];

			File_GetDirName(path, dirName, sizeof(dirName));
			File_GetFileName(path, fileName, sizeof(fileName));
			StrCat(fileName, sizeof(fileName), ".");

			Handle __dir = OpenDirectory(dirName);
			while (ReadDirEntry(__dir, dirEntry, sizeof(dirEntry))) {

				if (StrEqual(dirEntry, ".") || StrEqual(dirEntry, ".."))
					continue;
				if (strncmp(dirEntry, fileName, strlen(fileName)) == 0) {
					Format(dirEntry, sizeof(dirEntry), "%s/%s", dirName, dirEntry);
					File_AddToDownloadsTable(dirEntry, recursive, ignoreExts, size);
				}
			}
			CloseHandle(__dir);
		}
	}
	return;
}

stock bool File_ReadDownloadList(const char[] path)
{
	Handle file = OpenFile(path, "r");
	
	if (file  == INVALID_HANDLE)
		return false;

	char buffer[PLATFORM_MAX_PATH];
	while (!IsEndOfFile(file)) {
		ReadFileLine(file, buffer, sizeof(buffer));
		
		int pos;
		pos = StrContains(buffer, "//");
		if (pos != -1)
			buffer[pos] = '\0';
		
		pos = StrContains(buffer, "#");
		if (pos != -1)
			buffer[pos] = '\0';

		pos = StrContains(buffer, ";");
		if (pos != -1)
			buffer[pos] = '\0';
		
		TrimString(buffer);
		
		if (buffer[0] == '\0')
			continue;
		File_AddToDownloadsTable(buffer);
	}
	CloseHandle(file);
	return true;
}

stock void File_GetExtension(const char[] path, char[] buffer, int size)
{
	int extpos = FindCharInString(path, '.', true);
	
	if (extpos == -1) {
		buffer[0] = '\0';
		return;
	}

	strcopy(buffer, size, path[++extpos]);
}

stock int Array_FindString(const char[][] array, int size, const char[] str, bool caseSensitive=true, int start=0)
{
	if (start < 0)
		start = 0;
	for (int i=start; i < size; i++)
		if (StrEqual(array[i], str, caseSensitive))
			return i;
	return -1;
}

stock bool File_GetFileName(const char[] path, char[] buffer, int size)
{	
	if (path[0] == '\0') {
		buffer[0] = '\0';
		return;
	}
	
	File_GetBaseName(path, buffer, size);
	
	int pos_ext = FindCharInString(buffer, '.', true);

	if (pos_ext != -1)
		buffer[pos_ext] = '\0';
}

stock bool File_GetDirName(const char[] path, char[] buffer, int size)
{
	if (path[0] == '\0') {
		buffer[0] = '\0';
		return;
	}
	
	int pos_start = FindCharInString(path, '/', true);
	
	if (pos_start == -1) {
		pos_start = FindCharInString(path, '\\', true);
		
		if (pos_start == -1) {
			buffer[0] = '\0';
			return;
		}
	}
	strcopy(buffer, size, path);
	buffer[pos_start] = '\0';
}

stock bool File_GetBaseName(const char[] path, char[] buffer, int size)
{
	if (path[0] == '\0') {
		buffer[0] = '\0';
		return;
	}
	int pos_start = FindCharInString(path, '/', true);
	
	if (pos_start == -1)
		pos_start = FindCharInString(path, '\\', true);
	
	pos_start++;
	strcopy(buffer, size, path[pos_start]);
}